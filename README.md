# 1010!
1010! is a simple board game similar to Tetris. The objective is to fill rows and columns with the pieces provided and get the highest score. When a row or column is full, it is removed.

## Building
Run `mvn package` to build an executable jar file.
