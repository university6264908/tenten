package com.msatti.tenten;

/**
 * Represents a position on the board.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class Position {
	/**
	 * The x position.
	 */
	private final int x;
	
	/**
	 * The y position.
	 */
	private final int y;
	
	/**
	 * Constructs a new Position object.
	 * @param x The x-coordinate.
	 * @param y The y-coordinate.
	 */
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Returns the x position.
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Returns the y position.
	 */
	public int getY() {
		return y;
	}
}
