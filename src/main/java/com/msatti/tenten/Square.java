package com.msatti.tenten;

import java.awt.Color;

/**
 * Models squares on a 1010! board.
 * 
 * @author Lyndon While
 * @author Matthew Satti
 * @version 1.0
 */
public class Square {
	/**
	 * The background colour of the square.
	 */
    public static final Color BACKGROUND_COLOUR = Color.white; 
    
    /**
     * Whether the square is currently occupied.
     */
    private boolean isOccupied;
    
    /**
     * The current colour of the square.
     */
    private Color colour;

    /**
     * Constructs an empty square object.
     */
    public Square() {
        unsetSquare();
    }
    
    /**
     * Returns the colour of the square.
     */
    public Color getColour() {
        return colour;
    }

    /**
     * Returns whether the square is currently occupied.
     */
    public boolean isOccupied() {
        return isOccupied; 
    }
    
    /**
     * Set a square to occupied with the given colour.
     * @param colour The colour to set the square to.
     */
    public void setSquare(Color colour) {
    	isOccupied = true;
    	this.colour = colour;
    }
    
    /**
     * Sets a square to unoccupied with the background colour.
     */
    public void unsetSquare() {
    	isOccupied = false;
    	colour = BACKGROUND_COLOUR;
    }
}
