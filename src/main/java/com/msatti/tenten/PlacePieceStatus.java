package com.msatti.tenten;

/**
 * The possible scenarios when the player attempts to place a piece on the board.
 * @author Matthew Satti
 * @version 1.0
 */
public enum PlacePieceStatus {
	/**
	 * When an attempt to place a piece results with part of the piece being off-board.
	 */
    OFF_BOARD,
    
    /**
     * When an attempt to place a piece results with part of the piece overlapping another piece.
     */
    OCCUPIED,
    
    /**
     * When an attempt to place a piece is successful.
     */
    OK
}