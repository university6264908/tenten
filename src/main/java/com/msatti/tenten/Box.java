package com.msatti.tenten;

/**
 * Models piece-boxes for the 1010! game.
 * 
 * @author Lyndon While
 * @author Matthew Satti
 * @version 1.1
 */
public class Box {
    /**
     * Whether the box is occupied.
     */
    private boolean isOccupied;
    
    /**
     * A reference to the piece currently stored in the box.
     */
    private Piece piece; 

    /**
     * Constructs an empty box.
     */
    public Box() {
    	isOccupied = false;
        piece = null;
    }
    
    /**
     * Determines if the box is currently occupied.
     */
    public boolean isOccupied() {
        return isOccupied;
    }
    
    /**
     * Returns the current piece.
     */
    public Piece getPiece() {
        return piece;
    }

    /**
     * Consume the piece and set the box to empty.
     */
    public Piece useBox() {
    	isOccupied = false;
        return piece;
    }
    
    /**
     * Places a piece in the box.
     */
    public void fillBox(Piece p) {
    	isOccupied = true;
        piece = p;
    }
}
