package com.msatti.tenten;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class models pieces in 1010!.
 * 
 * @author Matthew Satti
 * @author Mitchell Cavanagh
 * @version 1.1
 */
public class Piece {
	/**
	 * The maximum width or height of any piece.
	 */
	public static int MAX_PIECE_SIZE = 5;
	
	/**
	 * An array of pieces to choose from.
	 */
	private static final Color[] COLOURS = {
			Color.RED,
			Color.GREEN,
			Color.BLUE,
			Color.MAGENTA,
			Color.CYAN,
			Color.YELLOW,
			Color.ORANGE,
			Color.PINK,
			Color.LIGHT_GRAY
	};
	
	/**
	 * A random object for selecting a colour.
	 */
	private static final Random random = new Random();
	
    /**
     * The positions for the squares in the piece.
     */
    private final List<Position> positions = new ArrayList<>();
    
    /**
     * The colour of the piece.
     */
    private final Color colour;
    
    /**
     * The number of squares in the x direction.
     */
    private int width = 0;
    
    /**
     * The number of squares in the y direction.
     */
    private int height = 0;

    /**
     * Constructs a new piece object.
     * For a given two-digit number XY maps to the coordinate (X, Y)
     * For a given one-difit number Y maps to the coordinate (0, Y)
     * The origin of these coordinates is at the top left.
     * @param positions The positions of the squares for the piece.
     */
    public Piece(int[] positions) {
        // Pick a random colour for this piece
        colour = getRandomColour();
        
        for (int position : positions) {
            // Get the coordinates of the piece squares from the integer
        	final int xPosition = position / 10;
        	final int yPosition = position % 10;
        	
        	// Update the piece size
        	if (xPosition + 1 > width) {
        		width = xPosition + 1;
        	}
        	if (yPosition + 1 > height) {
        		height = yPosition + 1;
        	}
        	
        	this.positions.add(new Position(xPosition, yPosition));
        }
    }
    
    /**
     * Returns the list of positions for this piece.
     */
    public List<Position> getPositions() {
        return positions;
    }

    /**
     * Returns the colour of this piece.
     */
    public Color getColour() {
        return colour;
    }

    /**
     * Returns the width of this piece.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of this piece.
     */
    public int getHeight() {
        return height;
    }
    
    /**
     * Returns a random colour to assign to this piece.
     */
    private static Color getRandomColour() {        
        return COLOURS[random.nextInt(COLOURS.length)];
    }
}
