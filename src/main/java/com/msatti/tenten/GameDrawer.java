package com.msatti.tenten;

import java.awt.Color;
import java.util.List;

/**
 * Draws the graphics for the 1010! game.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class GameDrawer {
	/**
	 * The size of the squares in pixels.
	 */
	public static final int SQUARE_SIZE = 25;
	
	/**
	 * The size of the gap between the boxes and the board in square units.
	 */
	public static final int BOX_BOARD_GAP = 1;
	
	/**
	 * The size of the gap between the boxes in square units.
	 */
	public static final int BOX_BOX_GAP = 1;
	
	/**
	 * The thickness of the border in pixels.
	 */
	public static final int BORDER_THICKNESS = 1;

	/**
	 * The default background colour.
	 */
	public static final Color BACKGROUND_COLOUR = Color.WHITE;
	
	/**
	 * The default grid colour.
	 */
	public static final Color GRID_COLOUR = Color.BLACK;
	
	/**
	 * Margin used to position the score in pixels.
	 */
	private static final int SCORE_MARGIN = 10;
		
	/**
	 * The canvas for this class to act upon.
	 */
	private final SimpleCanvas simpleCanvas;
	
	/**
	 * Constructs a new GameDrawer object.
	 * @param simpleCanvas The canvas to draw on.
	 */
	public GameDrawer(SimpleCanvas simpleCanvas) {
		this.simpleCanvas = simpleCanvas;
	}
	
	/**
	 * Draws the board.
	 */
	public void drawBoard() {
		drawGrid(GameState.BOARD_WIDTH, GameState.BOARD_HEIGHT, 0, 0);
	}
	
	/**
	 * Draws the boxes.
	 */
	public void drawBoxes() {
		for (int box = 0; box < GameState.NUMBER_OF_BOXES; box++) {
			final int xOffset = (GameState.BOARD_WIDTH + BOX_BOARD_GAP) * SQUARE_SIZE;
			final int yOffset = (Piece.MAX_PIECE_SIZE + BOX_BOX_GAP) * SQUARE_SIZE * box;
			drawGrid(Piece.MAX_PIECE_SIZE, Piece.MAX_PIECE_SIZE, xOffset, yOffset);
		}
	}
	
	/**
	 * Draws a piece inside a box.
	 * @param piece The piece to draw.
	 * @param box The box to draw in.
	 */
	public void drawPieceInBox(Piece piece, int box) {
		final int xOffset = (GameState.BOARD_WIDTH + BOX_BOARD_GAP) * SQUARE_SIZE;
		final int yOffset = (Piece.MAX_PIECE_SIZE + BOX_BOX_GAP) * SQUARE_SIZE * box;
		drawPiece(piece, xOffset, yOffset);
	}
	
	/**
	 * Undraws a piece from a box.
	 * @param piece The piece to undraw.
	 * @param box The box to undraw from.
	 */
	public void undrawPieceInBox(Piece piece, int box) {
		final int xOffset = (GameState.BOARD_WIDTH + BOX_BOARD_GAP) * SQUARE_SIZE;
		final int yOffset = (Piece.MAX_PIECE_SIZE + BOX_BOX_GAP) * SQUARE_SIZE * box;
		undrawPiece(piece, xOffset, yOffset);
	}
	
	/**
	 * Changes the border of the box.
	 * @param box The box to change the border of.
	 * @param color The colour to change it to.
	 */
	public void drawBoxBorder(int box, Color color) {
		makeBoxBorder(box, color);
	}
	
	/**
	 * Undraws the border to its default colour.
	 * @param box The box to change back.
	 */
	public void undrawBoxBorder(int box) {
		makeBoxBorder(box, GRID_COLOUR);
	}
	
	/**
	 * A generic method for changing a box border.
	 * @param box The box to change the border of.
	 * @param colour The colour to change it to.
	 */
	private void makeBoxBorder(int box, Color colour) {
		final int xOffset = (GameState.BOARD_WIDTH + BOX_BOARD_GAP) * SQUARE_SIZE;
		final int yOffset = (Piece.MAX_PIECE_SIZE + BOX_BOX_GAP) * SQUARE_SIZE * box;
		final int distanceToEnd = Piece.MAX_PIECE_SIZE * SQUARE_SIZE;
		
		// Top row
		simpleCanvas.drawLine(
			xOffset, 
			yOffset, 
			xOffset + distanceToEnd, 
			yOffset,
			colour
		);
		
		// Bottom row
		simpleCanvas.drawLine(
			xOffset, 
			yOffset + distanceToEnd, 
			xOffset + distanceToEnd, 
			yOffset + distanceToEnd,
			colour
		);
		
		// Left column
		simpleCanvas.drawLine(
			xOffset, 
			yOffset, 
			xOffset, 
			yOffset + distanceToEnd,
			colour
		);
		
		// Right column
		simpleCanvas.drawLine(
			xOffset + distanceToEnd, 
			yOffset, 
			xOffset + distanceToEnd, 
			yOffset + distanceToEnd,
			colour
		);
	}
	
	/**
	 * Draws a piece onto the playing board.
	 * @param piece The piece to draw.
	 * @param position The index of the square on the board.
	 */
	public void drawPieceOnBoard(Piece piece, Position position) {
		final int xOffset = position.getX() * SQUARE_SIZE;
		final int yOffset = position.getY() * SQUARE_SIZE;
		drawPiece(piece, xOffset, yOffset);
	}
	
	/**
	 * Undraws a single square on the board.
	 * @param position The index of the square on the board.
	 */
	public void undrawSquareOnBoard(Position position) {
		final int xOffset = position.getX() * SQUARE_SIZE;
		final int yOffset = position.getY() * SQUARE_SIZE;
		makeSquare(xOffset, yOffset, BACKGROUND_COLOUR);
	}
	
	/**
	 * A generic method for drawing a piece.
	 * @param piece The piece to draw.
	 * @param xOffset The x axis offset.
	 * @param yOffset The y axis offset.
	 */
	private void drawPiece(Piece piece, int xOffset, int yOffset) {
		List<Position> positions = piece.getPositions();
		for (Position position : positions) {
			makeSquare(
				(position.getX() * SQUARE_SIZE) + xOffset,
				(position.getY() * SQUARE_SIZE) + yOffset,
				piece.getColour()
			);
		}
	}
	
	/**
	 * A generic method for undrawing a piece.
	 * @param piece The piece to undraw.
	 * @param xOffset The x axis offset.
	 * @param yOffset The y axis offset.
	 */
	private void undrawPiece(Piece piece, int xOffset, int yOffset) {
		List<Position> positions = piece.getPositions();
		for (Position position : positions) {
			makeSquare(
				(position.getX() * SQUARE_SIZE) + xOffset,
				(position.getY() * SQUARE_SIZE) + yOffset,
				BACKGROUND_COLOUR
			);
		}
	}
	
	/**
	 * A generic method for drawing a square.
	 * @param xOffset The x axis offset.
	 * @param yOffset The y axis offset
	 * @param colour The colour of the square.
	 */
	private void makeSquare(int xOffset, int yOffset, Color colour) {
		simpleCanvas.drawRectangle(
			xOffset + BORDER_THICKNESS, 
			yOffset + BORDER_THICKNESS, 
			xOffset + SQUARE_SIZE, 
			yOffset + SQUARE_SIZE, 
			colour
		);
	}
	
	/**
	 * 
	 * @param finalScore
	 */
	public void drawGameOver(int finalScore) {
		simpleCanvas.drawString(
			"Game Over! Score: " + finalScore,
			SCORE_MARGIN, 
			GameState.BOARD_HEIGHT * SQUARE_SIZE + SCORE_MARGIN * 2, 
			GRID_COLOUR	
		);
	}
	
	/**
	 * Draws the score on the board.
	 * @param score The score to draw.
	 */
	public void drawScore(int score) {
		makeScore(score, GRID_COLOUR);
	}
	
	/**
	 * Undraws the score on the board.
	 * @param score The score to undraw.
	 */
	public void undrawScore(int score) {
		makeScore(score, BACKGROUND_COLOUR);
	}
	
	/**
	 * A generic method for drawing the score.
	 * @param score The score to draw.
	 * @param colour The colour of the score.
	 */
	private void makeScore(int score, Color colour) {
		simpleCanvas.drawString(
			"Score: " + score,
			SCORE_MARGIN, 
			GameState.BOARD_HEIGHT * SQUARE_SIZE + SCORE_MARGIN * 2, 
			colour	
		);
	}
	
	/**
	 * A generic method for drawing a grid.
	 * @param width The width of the grid.
	 * @param height The height of the grid.
	 * @param xOffset The x axis offset.
	 * @param yOffset The y axis offset.
	 */
	private void drawGrid(int width, int height, int xOffset, int yOffset) {
		// Draw rows
		for (int yPos = 0; yPos <= height; yPos++) {
			simpleCanvas.drawLine(
				xOffset, 							// x1
				(yPos * SQUARE_SIZE) + yOffset, 	// y1
				(width * SQUARE_SIZE) + xOffset, 	// x2
				(yPos * SQUARE_SIZE) + yOffset, 	// y2
				GRID_COLOUR
			);
		}
		
		// Draw columns
		for (int xPos = 0; xPos <= width; xPos++) {
			simpleCanvas.drawLine(
				(xPos * SQUARE_SIZE) + xOffset,		// x1
				yOffset,							// y1
				(xPos * SQUARE_SIZE) + xOffset,		// x2
				(height * SQUARE_SIZE) + yOffset,	// y2
				GRID_COLOUR
			);
		}
	}
}
