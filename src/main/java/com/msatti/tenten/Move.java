package com.msatti.tenten;

/**
 * Models a potential move in the 1010! game.
 *
 * @author Matthew Satti
 * @version 1.0
 */
public class Move {
	/**
	 * The box containing the piece for the move.
	 */
	private final Box box;
	
	/**
	 * The position on the board to place the piece.
	 * The top left corner is used as the origin.
	 */
	private final Position position;
	
	/**
	 * Constructs a new move object.
	 * @param box The box containing the piece.
	 * @param position The position on the board to place the piece.
	 */
	public Move(Box box, Position position) {
		this.box = box;
		this.position = position;
	}
	
	/**
	 * Returns the box.
	 */
	public Box getBox() {
		return box;
	}
	
	/**
	 * Returns the position.
	 */
	public Position getPosition() {
		return position;
	}
}
