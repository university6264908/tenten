package com.msatti.tenten;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

/**
 * This class models the 1010! interface.
 * 
 * @author Matthew Satti
 * @author Mitchell Cavanagh
 * @version 1.1
 */
public class Play1010 implements MouseListener  {
	/**
	 * All pieces that exist within this game.
	 */
	private static final Piece[] PIECES = {
		new Piece(new int[] {0, 1}),
	    new Piece(new int[] {0, 1, 2}),
	    new Piece(new int[] {0, 1, 2, 3}),
	    new Piece(new int[] {0, 1, 2, 3, 4}),
	    new Piece(new int[] {0, 10}),
	    new Piece(new int[] {0, 10, 20}),
	    new Piece(new int[] {0, 10, 20, 30}),
	    new Piece(new int[] {0, 10, 20, 30, 40}),
	    new Piece(new int[] {0}),
	    new Piece(new int[] {0, 1, 10, 11}),
	    new Piece(new int[] {0, 1, 2, 10, 11, 12, 20, 21, 22}),
	    new Piece(new int[] {0, 1, 2, 12, 22}),
	    new Piece(new int[] {0, 1, 11}),
	    new Piece(new int[] {10, 11, 1}),
	    new Piece(new int[] {20, 21, 22, 12, 2}),
	    new Piece(new int[] {2, 1, 0, 10, 20}),
	    new Piece(new int[] {1, 0, 10}),
	    new Piece(new int[] {0, 10, 11}),
	    new Piece(new int[] {0, 10, 20, 21, 22})
	};

	/**
	 * Tracks the current state of the game.
	 */
    private GameState gameState = new GameState();
    
    /**
     * The canvas for the player to interact with.
     */
    private SimpleCanvas simpleCanvas = new SimpleCanvas("1010!", 450, 450, GameDrawer.BACKGROUND_COLOUR);
    
    /**
     * The object that draws the GUI on the canvas.
     */
    private GameDrawer gameDrawer = new GameDrawer(simpleCanvas);
    
    /**
     * Generates random numbers for selecting next pieces.
     */
    private Random random = new Random();
        
    /**
     * The currently selected box.
     */
    private Integer selectedBox = null;
    
    /**
     * Implements an interactive human interface using mouse
     */
    public Play1010() {
        // Set up the viewport
    	simpleCanvas.addMouseListener(this);
    	
    	// Set up drawer
    	gameDrawer.drawBoard();
    	gameDrawer.drawBoxes();
    	gameDrawer.drawScore(gameState.getScore());
        getNextRoundOfPieces();
    }
    
    /**
     * Gets the next move ready when the player places a piece.
     */
    private void nextMove() {
        if (gameState.getOccupiedBoxes() == 0) {
            // This will fetch 3 random pieces.
            getNextRoundOfPieces();
        }
		
        if (gameState.getLegalMoves().isEmpty()) {
            // This will check if there are no moves left
        	gameDrawer.undrawScore(gameState.getScore());
            gameDrawer.drawGameOver(gameState.getScore());
        }
    }
    
    /**
     * Undraws the squares that have been cleared.
     */
    private void clearRowsAndColumns() {
    	for (int x = 0; x < GameState.BOARD_WIDTH; x++) {
    		for (int y = 0; y < GameState.BOARD_HEIGHT; y++) {
    			if (!gameState.getBoard()[x][y].isOccupied()) {
    				gameDrawer.undrawSquareOnBoard(new Position(x, y));
    			}
    		}
    	}
    }
    
    /**
     * Gets the next group of pieces into the boxes.
     */
    private void getNextRoundOfPieces() {
        for (int i = 0; i < 3; i++) {
        	int index = random.nextInt(PIECES.length);
        	Piece nextPiece = PIECES[index];
        	gameState.getBoxes()[i].fillBox(nextPiece);
        	gameDrawer.drawPieceInBox(nextPiece, i);
        }
    }
    
    /**
     * Handles any clicks inside the board.
     * @param e The mouse click event.
     */
    private void boardClick(MouseEvent e) {
    	// Check a box is selected
    	if (selectedBox == null) {
    		return;
    	}
    	
    	// Get the board cell
    	final int column = e.getX() / GameDrawer.SQUARE_SIZE;
    	final int row = e.getY() / GameDrawer.SQUARE_SIZE;
    	Position boardPosition = new Position(column, row);
    	Piece selectedPiece = gameState.getBoxes()[selectedBox].useBox();
    	
    	if (gameState.canPlacePiece(selectedPiece, boardPosition) == PlacePieceStatus.OK) {
    		// Remove the previous score
        	gameDrawer.undrawScore(gameState.getScore());
    		
        	// Place the piece on the board
    		gameState.placePiece(selectedPiece, boardPosition);
    		gameDrawer.drawPieceOnBoard(selectedPiece, boardPosition);
   
    		// Update the new state graphically
    		gameDrawer.undrawBoxBorder(selectedBox);
    		gameDrawer.undrawPieceInBox(selectedPiece, selectedBox);
        	clearRowsAndColumns();
        	gameDrawer.drawScore(gameState.getScore());
        	selectedBox = null;
        	
        	// Play the next move
        	nextMove();
    	} else {
    		gameState.getBoxes()[selectedBox].fillBox(selectedPiece);
    	}
    }
    
    /**
     * Handles any clicks inside a box.
     * @param box The index of the box clicked.
     */
    private void boxClick(int box) {
    	if (gameState.getBoxes()[box].isOccupied()) {
    		// Check a previous box wasnt selected
    		if (selectedBox != null) {
    			gameDrawer.undrawBoxBorder(selectedBox);
    		}
    		
    		selectedBox = box;
    		Color borderColour = gameState.getBoxes()[box].getPiece().getColour();
    		gameDrawer.drawBoxBorder(box, borderColour);
    	}
    }
    
    /**
     * Determines whether a mouse click was inside the board.
     * @param e The mouse click event.
     */
    private boolean clickWasInsideBoard(MouseEvent e) {
    	return e.getX() <= (GameState.BOARD_WIDTH * GameDrawer.SQUARE_SIZE) &&
    		   e.getY() <= (GameState.BOARD_HEIGHT * GameDrawer.SQUARE_SIZE);
    }
    
    /**
     * Determines whether a mouse click was inside a particular box.
     * @param e The mouse click event.
     * @param box The box index.
     */
    private boolean clickWasInsideBox(MouseEvent e, int box) {
    	return
    	// If x click was within x coordinates
    	(e.getX() >= (GameState.BOARD_WIDTH + GameDrawer.BOX_BOARD_GAP) * GameDrawer.SQUARE_SIZE &&
    	 e.getX() <= (GameState.BOARD_WIDTH + GameDrawer.BOX_BOARD_GAP + Piece.MAX_PIECE_SIZE) * GameDrawer.SQUARE_SIZE) &&
    	
    	// If y click was within y coordinates
    	(e.getY() >= (Piece.MAX_PIECE_SIZE + GameDrawer.BOX_BOX_GAP) * GameDrawer.SQUARE_SIZE * box &&
    	 e.getY() <= (Piece.MAX_PIECE_SIZE + GameDrawer.BOX_BOX_GAP) * GameDrawer.SQUARE_SIZE * box + (Piece.MAX_PIECE_SIZE * GameDrawer.SQUARE_SIZE));
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        // Determine where on the canvas the click was generated
    	if (clickWasInsideBoard(e)) {
    		boardClick(e);
    		// System.out.println("Board click");
    	} else {
    		for (int i = 0; i < GameState.NUMBER_OF_BOXES; i++) {
    			if (clickWasInsideBox(e, i)) {
    				boxClick(i);
    				// System.out.println("Box click at " + i);
    			}
    		}
    	}
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
    	
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    	
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    	
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    	
    }
}
