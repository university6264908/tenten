package com.msatti.tenten;

/**
 * Class to run the game.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class Program {
	/**
	 * Starts a new instance of the game.
	 */
	public static void main(String[] args) {
		new Play1010();
	}
}
