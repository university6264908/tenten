package com.msatti.tenten;

import java.util.ArrayList;
import java.util.List;

/**
 * This class models the 1010! set-up.
 * 
 * @author Matthew Satti
 * @author Mitchell Cavanagh
 * @version 1.1
 */
public class GameState {
    /**
     * The number of squares for the width of the board.
     */
    public static final int BOARD_WIDTH = 10;
    
    /**
     * The number of squares for the height of the board.
     */
    public static final int BOARD_HEIGHT = 10;

    /**
     * The number of boxes for the player to select pieces from.
     */
    public static final int NUMBER_OF_BOXES = 3;
    
    /**
     * The current state of the board.
     */
    private Square[][] board = new Square[BOARD_WIDTH][BOARD_HEIGHT];

    /**
     * The current state of the boxes.
     */
    private Box[] boxes = new Box[NUMBER_OF_BOXES];

    /**
     * The player's current score.
     */
    private int score = 0;

    /**
     * Constructs an empty GameState object.
     */
    public GameState() {
        // Initialise the board
        for (int xPos = 0; xPos < board.length; xPos++) {
            for (int yPos = 0; yPos < board[xPos].length; yPos++) {
                board[xPos][yPos] = new Square();
            }
        }
    
        // Initialise the boxes
        for (int i = 0; i < boxes.length; i++) {
            boxes[i] = new Box();
        }
    }
    
    /**
     * Returns the current state of the board.
     */
    public Square[][] getBoard() {
        return board;
    }
  
    /**
     * Returns the boxes in their current state.
     */
    public Box[] getBoxes() {
        return boxes;
    }
    
    /**
     * Returns the player's current score.
     */
    public int getScore() {
        return score;
    }
    
    /**
     * Returns the number of occupied boxes.
     */
    public int getOccupiedBoxes() {
    	int occupiedBoxes = 0;
    	for (Box box : boxes) {
    		if (box.isOccupied()) {
    			occupiedBoxes++;
    		}
    	}
    	return occupiedBoxes;
    }
  
    /**
     * Attempts to place a piece on the board. 
     * The status returned determines whether it was successful or not.
     * @param piece The piece to place.
     * @param position The position on the board to place the piece.
     */
    public PlacePieceStatus canPlacePiece(Piece piece, Position position) {
        // Test that the whole piece is within the board
        if (position.getX() + piece.getWidth() > BOARD_WIDTH || 
            position.getY() + piece.getHeight() > BOARD_HEIGHT) {
            return PlacePieceStatus.OFF_BOARD;
        }
        
        // Test the piece does not overlap another piece
        for (Position offset : piece.getPositions()) {
        	final int xPos = position.getX() + offset.getX();
        	final int yPos = position.getY() + offset.getY();
            if (board[xPos][yPos].isOccupied()) {
                return PlacePieceStatus.OCCUPIED;
            }
        }
        
        // If all passes, the piece can be placed
        return PlacePieceStatus.OK;
    }
    
    /**
     * Returns all legal moves at the current state.
     */
    public List<Move> getLegalMoves()
    {
        // ArrayList to return
        List<Move> moves = new ArrayList<>();
        
        // Iterate through boxes and test each square on the board
        for (int box = 0; box < NUMBER_OF_BOXES; box++) {
        	for (int xPos = 0; xPos < BOARD_WIDTH; xPos++) {
        		for (int yPos = 0; yPos < BOARD_HEIGHT; yPos++) {
        			if (boxes[box].isOccupied()) {
        				final Position position = new Position(xPos, yPos);
        				if (canPlacePiece(boxes[box].getPiece(), position) == PlacePieceStatus.OK) {
        					moves.add(new Move(boxes[box], position));
        				}
        			}
        		}
        	}
        }
        
        // Return ArrayList of moves
        return moves;
    }
    
    /**
     * Places a piece onto the board.
     * @param piece The piece to place.
     * @param position The position to place the piece.
     */
    public void placePiece(Piece piece, Position position)
    {
    	// Set the pieces to occupied
    	for (Position offset : piece.getPositions()) {
    		final int xPos = position.getX() + offset.getX();
        	final int yPos = position.getY() + offset.getY();
        	board[xPos][yPos].setSquare(piece.getColour());
    	}
    	
    	// Check columns and rows to be cleared
    	List<Integer> rowsToClear = checkRowsToClear();
    	List<Integer> columnsToClear = checkColumnsToClear();
    	
    	// Clear the rows and columns
    	for (int row : rowsToClear) {
    		clearRow(row);
    	}
    	
    	for (int column : columnsToClear) {
    		clearColumn(column);
    	}
    	
    	// Calculate the new score
    	final int scoreFromPlacingPiece = piece.getPositions().size();
    	final int scoreFromClearingRowsAndColumns = (rowsToClear.size() + columnsToClear.size()) * 10;
    	final int scoreFromRound = scoreFromPlacingPiece + scoreFromClearingRowsAndColumns;
    	score += scoreFromRound;
    	
    }
    
    /**
     * Returns a list of rows that can be cleared.
     */
    private List<Integer> checkRowsToClear() {
    	List<Integer> indices = new ArrayList<>();
    	
    	for (int yPos = 0; yPos < BOARD_WIDTH; yPos++) {
    		boolean canClear = true;
    		for (int xPos = 0; xPos < BOARD_HEIGHT; xPos++) {
    			if (!board[xPos][yPos].isOccupied()) {
    				canClear = false;
    				break;
    			}
    		}
    		
    		if (canClear) {
    			indices.add(yPos);
    		}
    	}
    	
    	return indices;
    }
    
    /**
     * Returns a list of columns that can be cleared.
     */
    private List<Integer> checkColumnsToClear() {
    	List<Integer> indices = new ArrayList<>();
    	
    	for (int xPos = 0; xPos < BOARD_WIDTH; xPos++) {
    		boolean canClear = true;
    		for (int yPos = 0; yPos < BOARD_HEIGHT; yPos++) {
    			if (!board[xPos][yPos].isOccupied()) {
    				canClear = false;
    				break;
    			}
    		}
    		
    		if (canClear) {
    			indices.add(xPos);
    		}
    	}
    	
    	return indices;
    }
    
    /**
     * Clears a row on the board.
     * @param index The row to clear.
     */
    private void clearRow(int index) {
    	for (int i = 0; i < BOARD_WIDTH; i++) {
    		board[i][index].unsetSquare();
    	}
    }
    
    /**
     * Clears a column on the board.
     * @param index The column to clear.
     */
    private void clearColumn(int index) {
    	for (int i = 0; i < BOARD_HEIGHT; i++) {
    		board[index][i].unsetSquare();
    	}
    }
}
